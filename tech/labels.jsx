﻿function clearAllLabels() {
    var layer = app.activeDocument.layers.getByName('type_names');
    var tfs = layer.textFrames;
    
    for (var i = 0; i < tfs.length; i++) {
        clearLabel(tfs[i]);
    }
 }

function clearLabel(textFrame) {
    textFrame.label = undefined;
}

function clearLabels(textFrames) {
    for (var i = 0; i < textFrames; i++) {
        clearLabel(textFrames[i]);
    }
}

function assignLabel(textFrame, labelText) {
    textFrame.label = labelText;
}