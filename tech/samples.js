// пишем в файл какое-то содержимое

var scriptFolderPath = File($.fileName).path; // the URI of the folder that contains the script file
var CSVFolderPath = scriptFolderPath + encodeURI("/tech/CSVFiles"); // the URI of the folder for your script resources
var CSVFile = new File(CSVFolderPath + encodeURI("/testcsv.xlsx"));

var content = generateContent(); // генерируем содержимое с помощью собственной функции

writeFile(CSVFile, content);

// функция записи в файл

function writeFile(fileObj, fileContent, encoding) {
    encoding = encoding || "utf-8";

    fileObj = (fileObj instanceof File) ? fileObj : new File(fileObj);

    var parentFolder = fileObj.parent;

    if (!parentFolder.exists && !parentFolder.create())
        throw new Error("Cannot create file in path " + fileObj.fsName);

    fileObj.encoding = encoding;

    fileObj.open("w");
    fileObj.write(fileContent);
    fileObj.close();

    return fileObj;
}

function replaceLineBreaks(array) {
    for (var i = 0; i < array.length; i++) {
        // обрезаем все пробелы и заменяем переносы строк на правильные
        array[i] = array[i].replace(/(^\s+|\s+$)/g, '');
        array[i] = array[i].replace(/(\r\n|\n|\r)/gm, '\n');
    }

    return array;
}

// array to array of arrays
function a_to_aoa(arr) {
  var aoa = [];

  for (var i = 0; i < arr.length; i++) {
      aoa[i] = [];
      aoa[i].push(arr[i]);
   }

    return aoa;
}

function getTextFramesContent(layerName) {
  var layer = app.activeDocument.layers.getByName(layerName);
  var tf = layer.textFrames;
  var tfContent = [];

  for  (i = 0; i < tf.length; i++) {
    var tfc = tf[i].contents;
    tfContent.push(tfc);
  }

  return tfContent;
}

function writeTableSecondColumn(arr) {
    var tablesFolder = Folder(tablesPath);
    //if (!tablesFolder.exists)
    //   tablesFolder.create();

    var tablePath = tablesPath + encodeURI('/numbers.xlsx');

    var wb = XLSX.utils.book_new();
    var ws = XLSX.utils.aoa_to_sheet([ ['Номер', 'Текст из исходного файла'] ]);
    arr = replaceLineBreaks(arr);
    var arrToAddColumn = a_to_aoa(arr);

    XLSX.utils.sheet_add_aoa(ws, arrToAddColumn, {origin: "B2"});
    XLSX.utils.book_append_sheet(wb, ws, 'NumbersTable');
    XLSX.writeFile(wb, tablePath);
}

function getTypeNames(layerName) {
    var layer = app.activeDocument.layers.getByName(layerName);
    var tf = layer.textFrames;

    var typeNames = []

    for (var i = 0; i < tf.length; i++) {
        var tfc = tf[i].contents;
        var number = getNumber(tfc);

        typeNames.push([number, tfc]);
    }

    return typeNames;
}

function getNumber(tfcontent) {
    return 'type';
}
