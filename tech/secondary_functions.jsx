﻿function includes(array, item) {
    for (var i = 0; i < array.length; i++) {
        if (array[i] == item) return true;
    }

    return false;
}

function colorNotMatch(textFrame) {
    var tr = textFrame.textRange;
    if (tr.length <= 0) return false;

    var color = tr.fillColor;
    if (color.black == 100 && color.cyan == 0 && color.magenta == 0 && color.yellow == 0) return false;
    return true;
}

function compareTableRows(row1, row2) {
    num1 = row1[0].split('_')[0];
    num2 = row2[0].split('_')[0];
    
    if (num1 == '') return 1;
    else if (num2 == '') return -1;
    else if (num1 == '_') return 1;
    else if (num2 == '_') return -1;
    else return num1 - num2;
}

function addNumbersToTableContent(content) {
    for (var i = 0; i < content.length; i++) {
        numberFrame = getConnectedTextFrame(content[i][3]);
        content[i].unshift(numberFrame.contents);
    }
    return content;
}

function compareWords(word1, word2) {
  if (word1 == '' && word2 == undefined || word2 == '' && word1 == undefined) return true;
  return word1 == word2;
}

function concatSymbolWord(symbol, word) {
    if (word == '') return symbol;
    if (symbol == '') return '';
    return symbol + '_' + word;
}

function getTextFrameById(uuid) {
    var layer = app.activeDocument.layers.getByName('type_names');
    var tfs = layer.textFrames;
    
    for (var i = 0; i < tfs.length; i++) {
        if (tfs[i].uuid == uuid) return tfs[i];
    }
    return undefined;
}

function getConnectedTextFrame(textFrame) {
    var layer = app.activeDocument.layers.getByName('type_names');
    var tfs = layer.textFrames;

    if (textFrame.note == '') return undefined;
    var text = textFrame.note.split(';')[1];
    for (var i = 0; i < tfs.length; i++) {
        if (text == tfs[i].contents) return tfs[i];
    }
}

function getLabels(tfsSorted) {
    var tfsLabels = [];
    for (var i = 0; i < tfsSorted.length; i++) {
        tfsLabels[i] = tfsSorted[i].slice(0, 4);
    }
    return tfsLabels;
}

function compareTf(tf1, tf2) {
    var pos1X = tf1[1], pos1Y = tf1[2];
    var pos2X = tf2[1], pos2Y = tf2[2];
    
    if (pos1Y < pos2Y) {
        return 1;
    } else if (pos1Y > pos2Y) {
        return -1;
    } else {
        if (pos1X > pos2X) {
            return 1;
        } else if (pos1X < pos2X) {
            return -1;
        } else {
            return 0;
        }
    }
}

function getRightSideRange(string) {
    var ind = string.indexOf('—');
    return string.slice(0, ind);
}

function writeTable(content) {
    var tablesFolder = Folder(tablesPath);
    
    var tablePath = tablesPath + encodeURI('/'+ namePrefix + getDatePostfix() + '.xlsx');

    var wb = XLSX.utils.book_new();
    var ws = XLSX.utils.aoa_to_sheet([ ['Номер', 'Текст из исходного файла'] ]);
    content = replaceLineBreaks_aoa(content);

    XLSX.utils.sheet_add_aoa(ws, content, {origin: "A2"});
    XLSX.utils.book_append_sheet(wb, ws, 'LabelsTable');
    XLSX.writeFile(wb, tablePath);
}

function getSymbolRows() {
    var tablesFolder = Folder(tablesPath);
    var tablePath = tablesPath + encodeURI('/Таблица соответствий для нумерации типов.ods');
    
    var wb = XLSX.readFile(tablePath, {cellDates:true});
    var ws = wb.Sheets['Symbols'];
    var data = XLSX.utils.sheet_to_json(ws, {header: 1});
    
    var result = [];
    for (var i = 1; i < data.length; i++) {
        if (data[i] == '') continue;
        result.push(data[i]);
    }
    return result;
}

function getSymbolWordRows() {
     var tablesFolder = Folder(tablesPath);
    var tablePath = tablesPath + encodeURI('/Таблица соответствий для нумерации типов.ods');
    
    var wb = XLSX.readFile(tablePath, {cellDates:true});
    var wsSymbol = wb.Sheets['Symbols'];
    var dataSymbol = XLSX.utils.sheet_to_json(wsSymbol, {header: 1});
    
    var wsWord = wb.Sheets['Vocabulary'];
    var dataWord = XLSX.utils.sheet_to_json(wsWord, {header: 1});
    
    var result = {
        'word': [],
        'symbol': []
    };

    for (var i = 1; i < dataSymbol.length; i++) {
        if (dataSymbol[i] == '') continue;
        result['symbol'].push(dataSymbol[i]);
    }

    for (var i = 1; i < dataWord.length; i++) {
        if (dataWord[i] == '') continue;
        result['word'].push(dataWord[i]);
    }

    return result;
}

function getWordRows() {
    var tablesFolder = Folder(tablesPath);
    var tablePath = tablesPath + encodeURI('/Таблица соответствий для нумерации типов.ods');
    
    var wb = XLSX.readFile(tablePath, {cellDates:true});
    var ws = wb.Sheets['Vocabulary'];
    var data = XLSX.utils.sheet_to_json(ws, {header: 1});
    
    var result = [];
    for (var i = 1; i < data.length; i++) {
        if (data[i] == '') continue;
        result.push(data[i]);
    }
    return result;
}

function getDatePostfix() {
    var date = new Date();
    var d = date.getDate();
    var m = date.getMonth() + 1;
    var hh = date.getHours();
    var mm = date.getMinutes();
    var y = date.getFullYear();

    function appendZero(m)
    { return (m < 10 ? '0' + m : m); }

    var datePostfix = y + '_' + appendZero(d) + '-' + appendZero(m) + '_' +
        appendZero(hh) + '-' + appendZero(mm);
    return datePostfix;
}

function replaceLineBreaks_aoa(array) {
    for (var i = 0; i < array.length; i++) {
        for (var j = 0; j < array[i].length; j++) {
            if (typeof array[i][j] != 'number') {
              array[i][j] = array[i][j].replace(/(^\s+|\s+$)/g, '');
              array[i][j] = array[i][j].replace(/(\r\n|\n|\r)/gm, '\n');
            }
        }
    }
    return array;
}

function checkIfNumber(textFrame) {
    if (textFrame.note.split(';')[0] == 'number')
        return true;
    else
        return false;
}