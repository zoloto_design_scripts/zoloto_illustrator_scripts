﻿// ----- базовые пути -----
var doc = app.activeDocument;
var basePath = File($.fileName).path;
var appPath = app.activeDocument.path;
var tablesPath = appPath + encodeURI('/Таблицы');
var tablesFolder = Folder(tablesPath);
var namePrefix = app.activeDocument.name.split('_')[0] + '_' + app.activeDocument.name.split('_')[1] + '_';
var MM_TO_POINTS = 2.83464567;


// ----- подключаем модуль для работы с таблицами -----
var xlsxPath = basePath + encodeURI('/node_modules/xlsx/dist/xlsx.extendscript.js');
var xlsxFile = File(xlsxPath);
if(xlsxFile.exists)
    $.evalFile(xlsxFile);
    
// ----- подключаем вспомогательные файлы -----
$.evalFile(basePath + encodeURI('/tech/labels.jsx'));
$.evalFile(basePath + encodeURI('/tech/secondary_functions.jsx'));

// ----- запуск основного скрипта -----
try { 
    //var symbolRows = getSymbolRows();
    //var wordRows = getWordRows();
    
    var symbolWordRows = getSymbolWordRows();
    
    var symbolRows = symbolWordRows['symbol'];
    var wordRows = symbolWordRows['word'];
    
    var dlg = new Window('dialog', 'wf_type_name_gen');
    dlg.orientation = 'column';
    
    // StaticText
    var titleDlg = dlg.add('statictext', undefined, 'Выберите опцию для нумерации скриптов');
    titleDlg.alignment = 'left';
    
    // Buttons
    var btns = dlg.add('group',);
    btns.alignChildren = ['fill', 'fill'];
    var primary = btns.add('button', undefined, 'Присвоить новую нумерацию');
    var secondary = btns.add('button', undefined, 'Скорректировать существующую нумерацию');
    secondary.onClick = secondaryNumbering;
    primary.onClick = primaryNumbering;
    
    dlg.center();
    dlg.show();
    
    //primaryNumbering ();
    
    function primaryNumbering() {
        var layer = app.activeDocument.layers.getByName('type_names');
        deleteNumberFrames(layer.textFrames);
    
        var tfs = layer.textFrames;
        var tfsSorted = sortTfs(tfs);
        tfsSorted = assignNumber(tfsSorted);    
        var tfsLabels = getLabels(tfsSorted);
    
        drawNumberFrames(tfsLabels, tfsSorted);
        var tfsLabelsSorted = sortTC(tfsLabels);
        writeTable(tfsLabelsSorted);
        dlg.close();
    }

    function secondaryNumbering() {
        var layer = app.activeDocument.layers.getByName('type_names');
        var numberFrames = [];
        var textFrames = [];
        var tfs = layer.textFrames;
        var tf;
        var tfsSorted = [];
        var dataSym;
        var tfsLabels;
        
        var symbolCounter = {};
        var symbol;
        var word;
        var numberFrame;
        
        var framesToRemove = [];
        var framesToUpdate = [];
        var status;
        
        var tableContentFrames = [];
        var tfsSortedTable = [];
        var tfsLabelsTable = [];
        
        // tc - table contents
        // просто массив всех фреймов с пометкой 'text'
        var tc_frames = [];
        // [ [ tf.contents, tf.left, tf.top, tf ], [ ... ], ... ]
        var tc_rowsTails = [];
        // [ [ number, tf.contents, tf.left, tf.top, tf ], [ ... ], ... ]
        var tc_extendedRows = [];
        // [ [ number, tf.contents, tf.left, tf.top, tf ], [ ... ], ... ]
        var tc_rows = [];
        // аналогично tc_rows, только отстортированные для записи в таблицу
        var tc_sortedRows = [];
        
        for (var i = 0; i < tfs.length; i++) {
            if (tfs[i].note.split(';')[0] == 'number') numberFrames.push(tfs[i]);
            else if (tfs[i].note.split(';')[0] == 'text') textFrames.push(tfs[i]);

            // если цвет не совпадает с #1d1d1b
            if (colorNotMatch(tfs[i])) {
                framesToUpdate.push(tfs[i]);
            }
        }
    
        for (var i = 0; i < symbolRows.length; i++) {
            symbol = symbolRows[i][1];
            symbolCounter[symbol] = getRightSideRange(symbolRows[i][0]) - 1;
        }
    
        for (var i = 0; i < numberFrames.length; i++) {
            symbol = numberFrames[i].contents.split('_')[1];
            // проходим и смотрим последнее число в каждом типе маркера
            if (symbolCounter[symbol] < numberFrames[i].contents.split('_')[0])
                symbolCounter[symbol] = numberFrames[i].contents.split('_')[0];

            // удаляем маркер, если было удалено название
            tf = getConnectedTextFrame(numberFrames[i]);
            if (tf == undefined) {
                numberFrames[i].remove();
            }
        }
    
        for (var i = 0; i < textFrames.length; i++) {
            // проверяем символ, если изменился - добавляем в framesToUpdate
            // если символ такой же, проверяем слово, если изменилось - просто меняем слово в соответствующем фрейме
            // двигаем фрейм
            
            symbol = getSymbol(textFrames[i].contents);
            word = getWord(textFrames[i].contents);
            numberFrame = getConnectedTextFrame(textFrames[i]);
            if (numberFrame == undefined) {
                continue;
            }
            if (symbol != numberFrame.contents.split('_')[1] && includes(framesToUpdate, textFrames[i]))  {
                numberFrame.remove();
                continue;
            } else if (word != numberFrame.contents.split('_')[2] && includes(framesToUpdate, textFrames[i])) {
                numberFrame.contents = numberFrame.contents.split('_')[0] + '_' +
                                                      concatSymbolWord(numberFrame.contents.split('_')[1], word);
            }
            
            X = textFrames[i].left;
            Y = textFrames[i].top + 30,2 * MM_TO_POINTS;
            numberFrame.position = [X, Y + 10];
            numberFrame.font = 'Arial';
            numberFrame.textRange.characterAttributes.size = 24;
        }
    
        // добавить в tfsSorted, переделать функции из primaryNumbering
        // например, функцию assignNumber в assignNumberSecondary, чтобы добавить нужный symbolCounter

        tfsSorted = sortTfs(framesToUpdate);
    
        for (var i = 0; i < symbolRows.length; i++) {
            var symbol = symbolRows[i][1];
            symbolCounter[symbol] = +symbolCounter[symbol] + 1;
        }
    
        for (var i = 0; i < tfsSorted.length; i++) {
            dataSym = getSymbolWithCounter(tfsSorted[i][0], symbolCounter);
            symbol = dataSym['symbol'];
            symbolCounter = dataSym['symbolCount'];
            word = getWord(tfsSorted[i][0]);
        
            tfsSorted[i].unshift(concatSymbolWord(symbol, word));            
        }
        tfsLabels = getLabels(tfsSorted);    
        drawNumberFrames(tfsLabels, tfsSorted);
        
        // добавляем данные в таблицу
        for (var i = 0; i < tfs.length; i++) {
            if (tfs[i].note.split(';')[0] == 'text') tc_frames.push(tfs[i]);
        }
        tc_rowsTails = sortTfs(tc_frames);
        tc_extendedRows = addNumbersToTableContent(tc_rowsTails);
        tc_rows = getLabels(tc_extendedRows);
        tc_sortedRows = sortTC(tc_rows);
        writeTable(tc_sortedRows);
        
        dlg.close();  
    }
} catch (e) {
    alert(e);
}

function sortTC(rows) {
    var p;
    
    for (var i = 0; i < rows.length - 1; i++) {
        for (var j = 0; j < rows.length - 1 - i; j++) {
            if (compareTableRows(rows[j], rows[j + 1]) > 0) {
                p = rows[j];
                rows[j] = rows[j + 1];
                rows[j + 1] = p;
            }
        }
    }
    return rows;
}

function sortTfs(textframes) {
    var sorted = [];
    
    for (var i = 0; i < textframes.length; i++) {
        var tf = textframes[i];
        if (tf.contents == '') continue;
        var arr = [tf.contents, tf.left, tf.top, tf]
        sorted.push(arr);
    }

    var p;
    for (var i = 0; i < sorted.length - 1; i++) {
        for (var j = 0; j < sorted.length - 1 - i; j++) {
            if (compareTf(sorted[j], sorted[j + 1]) > 0) {
                p = sorted[j];
                sorted[j] = sorted[j + 1];
                sorted[j + 1] = p;
            }
        }
    }

    return sorted;
 }

function assignNumber(textframesSorted) {
    var symbolCounter = {};
    
    for (var i = 0; i < symbolRows.length; i++) {
        var symbol = symbolRows[i][1];
        symbolCounter[symbol] = getRightSideRange(symbolRows[i][0]);
    }
    
    var dataSym, symbol;
    for (var i = 0; i < textframesSorted.length; i++) {
        dataSym = getSymbolWithCounter(textframesSorted[i][0], symbolCounter);
        symbol = dataSym['symbol'];
        symbolCounter = dataSym['symbolCount'];
        
        word = getWord(textframesSorted[i][0]);
        
        textframesSorted[i].unshift(concatSymbolWord(symbol, word));
    }

    return textframesSorted;    
 }

function deleteNumberFrames(textFrames) {    
    numberTfs = [];
    for (var i = 0; i < textFrames.length; i++) {
        if (checkIfNumber(textFrames[i]))
            numberTfs.push(textFrames[i]);
    }
    
    for (var j = 0; j < numberTfs.length; j++) {
        numberTfs[j].remove();
    }
}

function drawNumberFrames(textframesLabels, textframesSorted) {
    var layer = app.activeDocument.layers.getByName('type_names');    
    var tagList, typeTag, uuidTag;
    
    var X, Y, width, height, pathItem, tf;
    for (var i = 0; i < textframesLabels.length; i++) {
        X = textframesLabels[i][2];
        Y = textframesLabels[i][3] + 30,2 * MM_TO_POINTS;
        width =  176 * MM_TO_POINTS;
        height = 10,2417 * MM_TO_POINTS;
        
        var numberFrame = layer.textFrames.add();       
        numberFrame.position = [X, Y];
        numberFrame.font = 'Arial';
        numberFrame.textRange.characterAttributes.size = 24;
        numberFrame.contents = textframesLabels[i][0];
        
        numberFrame.note = 'number;' + textframesSorted[i][4].contents;
        textframesSorted[i][4].note = 'text;' + numberFrame.contents;
    }
}

function getWord(text) {
    text = text.toLowerCase();
    var row, element;
    for (var i = 0; i < wordRows.length; i++) {
        row = wordRows[i];
        for (var j = 2; j < row.length; j++) {
            element = row[j];
            if (text.indexOf(element.toLowerCase()) != -1) {
                return row[1];
            }
        }
    }

    return '';
}

function getSymbolWithCounter(text, symbolCount) {
    text = text.toLowerCase();
    var result = {}, row, element;
    for (var i = 0; i < symbolRows.length; i++) {
        row = symbolRows[i];
        for (var j = 2; j < row.length; j++) {
            element = row[j];
            if (text.indexOf(element.toLowerCase()) != -1) {
                result['symbol'] = symbolCount[row[1]] + '_' + row[1];
                symbolCount[row[1]] = +symbolCount[row[1]] + 1;
                result['symbolCount'] = symbolCount;
                return result;
            }
        }
    }

    result['symbol'] = '';
    result['symbolCount'] = symbolCount;
    return result;
}

function getSymbol(text) {
    var row, element;
    
    text = text.toLowerCase();
    for (var i = 0; i < symbolRows.length; i++) {
        row = symbolRows[i];
        for (var j = 2; j < row.length; j++) {
            element = row[j];
            if (text.indexOf(element.toLowerCase()) != -1) return row[1];
        }
    }
    return '';
}
